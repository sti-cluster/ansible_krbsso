KRBSSO
=========

Installs and configures Active Directory on Linux using sssd and kerberos.

Compatibility
------------

This role is compatible with
  - Ubuntu 18
  - CentOS 7/8
  - Redhat 7/8

Requirements
------------

This role require a user keytab that can be generated using the following instruction:

First, you need the key version number of the domain account you want to use for keytab credentials

Please replace ad_join_username with the corresponding one that have permissions to join the domain controler
>**NOTE**: Check out for default variable **ad_join_username** value in **Role Variables** section below for details

```bash
ldapsearch -x -h intranet.epfl.ch -b "dc=intranet,dc=epfl,dc=ch" -D "CN=ad_join_username,OU=ComptesDeGestion,DC=intranet,DC=epfl,DC=ch" -W '(sAMAccountName=ad_join_username)' msDS-KeyVersionNumber
```

Currently, the key version is 2, so use it accordingly in the ktuil command

```
$ ktutil
ktutil:  addent -password -p ad_join_username@INTRANET.EPFL.CH -k 2 -e aes256-cts-hmac-sha1-96
Password for ad_join_username@INTRANET.EPFL.CH: <enter the password>
ktutil:  wkt /etc/krb5.keytab.ad_join_username
ktutil:  q 
```

if you use a Mac to generate the keytab, use the following

```
ktutil -k ad_join_username.keytab add -p ad_join_username@INTRANET.EPFL.CH -e aes256-cts-hmac-sha1-96 -V 2
```

After the keytab has been successfully created, you can check the file with klist

```
klist -k -t /etc/krb5.keytab.ad_join_username
```

Or on a Mac, use the following to verify your keytab
```
ktutil -k ad_join_username.keytab list
```

Then try to initialise it to make sure it is working properly

```
kinit ad_join_username@INTRANET.EPFL.CH -k -t /etc/krb5.keytab.ad_join_username
```

You can now store this file inside the folder **files** for this roles

Role Variables
--------------

**Default variables**

| Variable          | Default Value           | Type   | Description |
| ----------------- |:-----------------------:| :-----:| ----------- |
| domain_name       | epfl.ch | string | Company domain name |
| realm_short       | intranet | string | Active Directory domain name |
| dns_serverip      | 128.178.15.227, 128.178.15.228, 128.178.15.229 | array | Active Directory DNS servers |
| ntp_servers       | 0.ch.pool.ntp.org, 1.ch.pool.ntp.org, 2.ch.pool.ntp.org, 3.ch.pool.ntp.org | array | Timer servers address |
| localtime_region  | Europe/Zurich        | string | Set time region based on /usr/share/zoneinfo/ |
| default_allowed_admin_groups   | sti-it_appgrpu, sti-it-staffu | array | Define admin groups that can access the server and that will be added to sudoers list |
| default_allowed_groups | NULL | array | Define groups that can access the server |
| default_shell | /bin/bash | string | User default shell |
| default_homedir | /home/%u | string | User default home directory |
| ad_base_dn | DC={{ realm_short }},DC={{ domain_name.split('.')[0] }},DC={{ domain_name.split('.')[1] }} | string | Base DN of your Active Directory |
| ad_default_computers_ou | OU=STI-Cluster,OU=STI-IT-Servers,OU=STI-IT,OU=STI-SG,OU=STI,{{ ad_base_dn }} | string | Where to store newly added computer to |
| ad_join_username | stiitcomputermgr | string | User account that have machine creation privileges |
| ad_join_userkeytab | /etc/krb5.{{ ad_join_username }}.keytab | string | Where to store the key tab file that will be used to add the computer to the Active Directory |
| ad_force_rejoin | false | boolean | Force rejoin the Active Directory domain |
| sshd_permit_root_login | false | boolean | Allow root login with password |
| sudoer_filename | {{ realm_short }}-default | string | Default sudoer file name that will be used to add admins and normal users to |
| sudoer_add_default_allowed_groups | false | boolean | Allow normal users from default_allowed_groups to have full sudo access |
| sudoer_allow_nopassword | false | boolean | Allow sudo commands to be executed without a password |
| mkhomedir_enabled | false | boolean | Disable or Enable automatic user's home directory creation |



Dependencies
------------

N/A

Example Playbook
----------------

```yaml
---
- hosts: all
  roles:
    - roles/krbsso
```

License
-------

MIT

Author Information
------------------

Written by [Mikael Doche](mailto:mikael.doche@epfl.ch) for EPFL - STI school of engineering